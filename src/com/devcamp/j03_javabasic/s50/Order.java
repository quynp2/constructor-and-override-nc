package com.devcamp.j03_javabasic.s50;

import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua

    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 1200000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "Book", "Pen", "Rule" };
    }

    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = new String[] { "Book", "Pen", "Rule" };
    }

    public Order() {
        this("2");
    }

    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] { "Book", "La", "Ca", "Rau" });

    }

    @Override
    public String toString() {
        Locale.setDefault(new Locale("vi", "VN"));
        String patern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(patern);
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        return "Order [Id= " + id
                + ", Customer Name = " + customerName
                + ", Price = " + usNumberFormat.format(price)
                + ", Oder Date = "
                + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", Confirm = " + confirm
                + ", items = " + Arrays.toString(items);

    }

}
