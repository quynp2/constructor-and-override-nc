import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03_javabasic.s50.Order;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrayList = new ArrayList<>();
        // khởi tạo order với các tham số khác nhau
        Order order1 = new Order();
        Order order2 = new Order("Lan");
        Order order3 = new Order(2, "Nguyen thi phuong lan", 200000);
        Order order4 = new Order(3, "Tran Ngoc My Hoa", 1, new Date(), true, new String[] { "Lua", "Doi", "Dien" });
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);
        arrayList.add(order4);
        for (Order order : arrayList) {
            System.out.println(order.toString());
        }

    }
}
